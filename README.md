## CFilter -- A simple Path of Exile item filter.
This is a simple item filter created for [Path of Exile](https://pathofexile.com). The look is inspired by one of the most popular filters: [Greengroove's Item Filter](https://github.com/Greengroove/GG-LootFilter), but the structure is much more basic. If you are looking for a more feature-rich filter use something else. 

*This filter is meant to be used by people that understand how to add and edit filter rules by hand and **is not** directly compatible for customization with Filterblade, Filterblast, or Filtration.*

## Installation
 1. You can download the latest version of the filter [here](https://bitbucket.org/sgoll/itemfilter/downloads/?tab=tags).
 2. Unzip the file and copy CFilter.filter to \Documents\My Games\Path of Exile.
 3. Inside of the game, go to Options > UI > List of Item Filters.
 4. Select CFilter.

## Editing
To edit the filter, you must use a programming text editor that is able to save the file with UTF-8 encoding. The filter has a list of sections that you can edit by searching the filter file for the particular section and then adding/editing/removing rules. 

## Economy
There is a small Python program included with the filter that when ran, will generate a list of divination cards and uniques based on the current league's economy. These lists can then be updated in the filter file to keep the highlighting for valuable cards and uniques inline with the current meta. 