import urllib.request, json, time

league = 'Sentinel'

# -------------------------------------------------------------------
# Divination Cards
# -------------------------------------------------------------------
start = time.time()
print('Fetching list of divination cards from poe.ninja')

req = urllib.request.Request('https://poe.ninja/api/data/itemoverview?league=' + league + '&type=DivinationCard', headers={'User-Agent': 'Mozilla/5.0'})
r = urllib.request.urlopen(req).read()
d = json.loads(r)

high_tier = 0
mid_tier = 0
low_tier = 0
trash_tier = 0

high_str = ''
mid_str = ''
low_str = ''

for i in d['lines']:
    if i['chaosValue'] >= 20:
        high_tier += 1
        high_str += '"' + i['name'] + '" '
    elif i['chaosValue'] >= 10 and i['chaosValue'] < 20:
        mid_tier += 1
        mid_str += '"' + i['name'] + '" '
    elif i['chaosValue'] >= 3 and i['chaosValue'] < 10:
        low_tier += 1
        low_str += '"' + i['name'] + '" '
    else:
        trash_tier += 1

total = high_tier + mid_tier + low_tier + trash_tier
f = open('econ/cards.txt', 'w')
f.write(high_str.strip() + "\n\n")
f.write(mid_str.strip() + "\n\n")
f.write(low_str.strip() + "\n\n")

f.write('There are {0} cards worth more than 20c, {1} cards worth between 10 and 20c, and {2} cards worth between 3 and 10c\n'.format(high_tier, mid_tier, low_tier))
f.write('There are {0} cards worth less than 3c\n'.format(trash_tier))
f.write('There are {0} cards listed'.format(total))
f.close()

end = time.time()
elapsed = end - start


print('Finished in {:+.2f}s, check cards.txt'.format(elapsed))

# -------------------------------------------------------------------
# Fossils
# -------------------------------------------------------------------
start = time.time()
print('Fetching list of fossils from poe.ninja')

req = urllib.request.Request('https://poe.ninja/api/data/itemoverview?league=' + league + '&type=Fossil', headers={'User-Agent': 'Mozilla/5.0'})
r = urllib.request.urlopen(req).read()
d = json.loads(r)

high_tier = 0
mid_tier = 0
ignore = 0

high_str = ''
mid_str = ''

for i in d['lines']:
    if i['chaosValue'] >= 20:
        high_tier += 1
        high_str += '"' + i['name'] + '" '
    elif i['chaosValue'] >= 3 and i['chaosValue'] < 20:
        mid_tier += 1
        mid_str += '"' + i['name'] + '" '
    else:
        ignore += 1

f = open('econ/fossils.txt', 'w')
f.write(high_str.strip() + "\n\n")
f.write(mid_str.strip() + "\n\n")

f.write('There are {0} fossils worth more than 20c, and {1} fossils worth between 3 and 20c\n'.format(high_tier, mid_tier))
f.write('There were {0} fossils ignored'.format(ignore))

end = time.time()
elapsed = end - start

print('Finished in {:+.2f}s check fossils.txt'.format(elapsed))

# -------------------------------------------------------------------
# Scarabs
# -------------------------------------------------------------------
start = time.time()
print('Fetching list of scarabs from poe.ninja')

req = urllib.request.Request('https://poe.ninja/api/data/itemoverview?league=' + league + '&type=Scarab', headers={'User-Agent': 'Mozilla/5.0'})
r = urllib.request.urlopen(req).read()
d = json.loads(r)

high_tier = 0
mid_tier = 0
ignore = 0

high_str = ''
mid_str = ''

for i in d['lines']:
    if i['chaosValue'] >= 20:
        high_tier += 1
        high_str += '"' + i['name'] + '" '
    elif i['chaosValue'] >= 3 and i['chaosValue'] < 20:
        mid_tier += 1
        mid_str += '"' + i['name'] + '" '
    else:
        ignore += 1

f = open('econ/scarabs.txt', 'w')
f.write(high_str.strip() + "\n\n")
f.write(mid_str.strip() + "\n\n")

f.write('There are {0} scarabs worth more than 20c, and {1} scarabs worth between 3 and 20c\n'.format(high_tier, mid_tier))
f.write('There were {0} scarabs ignored'.format(ignore))

end = time.time()
elapsed = end - start

print('Finished in {:+.2f}s check scarabs.txt'.format(elapsed))

# -------------------------------------------------------------------
# Delirium Orbs
# -------------------------------------------------------------------
start = time.time()
print('Fetching list of delirium orbs from poe.ninja')

req = urllib.request.Request('https://poe.ninja/api/data/itemoverview?league=' + league + '&type=DeliriumOrb', headers={'User-Agent': 'Mozilla/5.0'})
r = urllib.request.urlopen(req).read()
d = json.loads(r)

high_tier = 0
mid_tier = 0
ignore = 0

high_str = ''
mid_str = ''

for i in d['lines']:
    if i['chaosValue'] >= 20:
        high_tier += 1
        high_str += '"' + i['name'] + '" '
    elif i['chaosValue'] >= 3 and i['chaosValue'] < 20:
        mid_tier += 1
        mid_str += '"' + i['name'] + '" '
    else:
        ignore += 1

f = open('econ/delirium.txt', 'w')
f.write(high_str.strip() + "\n\n")
f.write(mid_str.strip() + "\n\n")

f.write('There are {0} delirium orbs worth more than 20c, and {1} delirium orbs worth between 3 and 20c\n'.format(high_tier, mid_tier))
f.write('There were {0} delirium orbs ignored'.format(ignore))

end = time.time()
elapsed = end - start

print('Finished in {:+.2f}s check delirium.txt'.format(elapsed))

# -------------------------------------------------------------------
# Oils
# -------------------------------------------------------------------
start = time.time()
print('Fetching list of oils from poe.ninja')

req = urllib.request.Request('https://poe.ninja/api/data/itemoverview?league=' + league + '&type=Oil', headers={'User-Agent': 'Mozilla/5.0'})
r = urllib.request.urlopen(req).read()
d = json.loads(r)

high_tier = 0
mid_tier = 0
ignore = 0

high_str = ''
mid_str = ''

for i in d['lines']:
    if i['chaosValue'] >= 20:
        high_tier += 1
        high_str += '"' + i['name'] + '" '
    elif i['chaosValue'] >= 3 and i['chaosValue'] < 20:
        mid_tier += 1
        mid_str += '"' + i['name'] + '" '
    else:
        ignore += 1

f = open('econ/oils.txt', 'w')
f.write(high_str.strip() + "\n\n")
f.write(mid_str.strip() + "\n\n")

f.write('There are {0} oils worth more than 20c, and {1} oils worth between 3 and 20c\n'.format(high_tier, mid_tier))
f.write('There were {0} oils ignored'.format(ignore))

end = time.time()
elapsed = end - start

print('Finished in {:+.2f}s check oils.txt'.format(elapsed))

# -------------------------------------------------------------------
# Awakened Gems
# -------------------------------------------------------------------
start = time.time()
print('Fetching list of awakened gems from poe.ninja')

req = urllib.request.Request('https://poe.ninja/api/data/itemoverview?league=' + league + '&type=SkillGem', headers={'User-Agent': 'Mozilla/5.0'})
r = urllib.request.urlopen(req).read()
d = json.loads(r)

high_tier = 0
mid_tier = 0
ignore = 0

high_list = []
mid_list = []

high_str = ''
mid_str = ''

for i in d['lines']:
    if "Awakened" not in i['name']:
        continue
    if i['gemLevel'] > 1:
        continue
    if i['chaosValue'] >= 50:
        high_tier += 1
        high_list.append(i['name'].removesuffix('Support').strip())
    elif i['chaosValue'] >= 20 and i['chaosValue'] < 50:
        mid_tier += 1
        mid_list.append(i['name'].removesuffix('Support').strip())
    else:
        ignore += 1

high_list = list(set(high_list))
mid_list = list(set(mid_list))
high_list.sort()
mid_list.sort()

f = open('econ/gems.txt', 'w')
f.write(' '.join('"{0}"'.format(w) for w in high_list).strip() + "\n\n")
f.write(' '.join('"{0}"'.format(w) for w in mid_list).strip() + "\n\n")

f.write('There are {0} gems worth more than 50c, and {1} gems worth between 20 and 50c\n'.format(high_tier, mid_tier))
f.write('There were {0} gems ignored'.format(ignore))

end = time.time()
elapsed = end - start

print('Finished in {:+.2f}s check gems.txt'.format(elapsed))

# -------------------------------------------------------------------
# Bases
# -------------------------------------------------------------------
start = time.time()
print('Fetching list of bases from poe.ninja')

req = urllib.request.Request('https://poe.ninja/api/data/itemoverview?league=' + league + '&type=BaseType', headers={'User-Agent': 'Mozilla/5.0'})
r = urllib.request.urlopen(req).read()
d = json.loads(r)

normal = 0
shaper = 0
elder = 0

# conquerors
crusader = 0
hunter = 0
redeemer = 0
warlord = 0

normal_ignore = 0
shaper_ignore = 0
elder_ignore = 0
confidence_ignore = 0

# conquerors
crusader_ignore = 0
hunter_ignore = 0
redeemer_ignore = 0
warlord_ignore = 0

normal_str_83 = ''
normal_str_84 = ''
normal_str_85 = ''
normal_str_86 = ''

shaper_str_83 = ''
shaper_str_84 = ''
shaper_str_85 = ''
shaper_str_86 = ''

elder_str_83 = ''
elder_str_84 = ''
elder_str_85 = ''
elder_str_86 = ''

# conquerors
crusader_str_83 = ''
crusader_str_84 = ''
crusader_str_85 = ''
crusader_str_86 = ''

hunter_str_83 = ''
hunter_str_84 = ''
hunter_str_85 = ''
hunter_str_86 = ''

redeemer_str_83 = ''
redeemer_str_84 = ''
redeemer_str_85 = ''
redeemer_str_86 = ''

warlord_str_83 = ''
warlord_str_84 = ''
warlord_str_85 = ''
warlord_str_86 = ''

for i in d['lines']:
    if i['count'] < 10:
        confidence_ignore += 1

print('-- normal base list...')
for i in d['lines']:
    if i['count'] >= 10 and 'variant' not in i.keys():
        if i['chaosValue'] >= 20:
            normal += 1

            if i['levelRequired'] == 83:
                normal_str_83 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 84:
                normal_str_84 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 85:
                normal_str_85 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 86:
                normal_str_86 += '"' + i['name'] + '" '
        else:
            normal_ignore += 1

print('-- elder base list...')
for i in d['lines']:
    if i['count'] >= 10 and 'variant' in i.keys() and i['variant'] == 'Elder':
        if i['chaosValue'] >= 20:
            elder += 1

            if i['levelRequired'] == 83:
                elder_str_83 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 84:
                elder_str_84 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 85:
                elder_str_85 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 86:
                elder_str_86 += '"' + i['name'] + '" '
        else:
            elder_ignore += 1

print('-- shaper base list...')
for i in d['lines']:
    if i['count'] >= 10 and 'variant' in i.keys() and i['variant'] == 'Shaper':
        if i['chaosValue'] >= 20:
            shaper += 1

            if i['levelRequired'] == 83:
                shaper_str_83 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 84:
                shaper_str_84 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 85:
                shaper_str_85 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 86:
                shaper_str_86 += '"' + i['name'] + '" '
        else:
            shaper_ignore += 1

print('-- crusader base list...')
for i in d['lines']:
    if i['count'] >= 10 and 'variant' in i.keys() and i['variant'] == 'Crusader':
        if i['chaosValue'] >= 20:
            crusader += 1

            if i['levelRequired'] == 83:
                crusader_str_83 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 84:
                crusader_str_84 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 85:
                crusader_str_85 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 86:
                crusader_str_86 += '"' + i['name'] + '" '
        else:
            crusader_ignore += 1

print('-- hunter base list...')
for i in d['lines']:
    if i['count'] >= 10 and 'variant' in i.keys() and i['variant'] == 'Hunter':
        if i['chaosValue'] >= 20:
            hunter += 1

            if i['levelRequired'] == 83:
                hunter_str_83 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 84:
                hunter_str_84 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 85:
                hunter_str_85 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 86:
                hunter_str_86 += '"' + i['name'] + '" '
        else:
            hunter_ignore += 1

print('-- redeemer base list...')
for i in d['lines']:
    if i['count'] >= 10 and 'variant' in i.keys() and i['variant'] == 'Redeemer':
        if i['chaosValue'] >= 20:
            redeemer += 1

            if i['levelRequired'] == 83:
                redeemer_str_83 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 84:
                redeemer_str_84 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 85:
                redeemer_str_85 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 86:
                redeemer_str_86 += '"' + i['name'] + '" '
        else:
            redeemer_ignore += 1

print('-- warlord base list...')
for i in d['lines']:
    if i['count'] >= 10 and 'variant' in i.keys() and i['variant'] == 'Warlord':
        if i['chaosValue'] >= 20:
            warlord += 1

            if i['levelRequired'] == 83:
                warlord_str_83 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 84:
                warlord_str_84 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 85:
                warlord_str_85 += '"' + i['name'] + '" '
            elif i['levelRequired'] == 86:
                warlord_str_86 += '"' + i['name'] + '" '
        else:
            warlord_ignore += 1

f = open('econ/bases.txt', 'w')

f.write('# shaper bases' + "\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Shaper' + "\n")
f.write('\tItemLevel >= 86' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + shaper_str_86.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 124 77 255 255' + "\n")
f.write('\tSetBackgroundColor 124 77 255 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Shaper' + "\n")
f.write('\tItemLevel = 85' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + shaper_str_85.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 124 77 255 255' + "\n")
f.write('\tSetBackgroundColor 124 77 255 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Shaper' + "\n")
f.write('\tItemLevel = 84' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + shaper_str_84.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 124 77 255 255' + "\n")
f.write('\tSetBackgroundColor 124 77 255 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Shaper' + "\n")
f.write('\tItemLevel = 83' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + shaper_str_83.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 124 77 255 255' + "\n")
f.write('\tSetBackgroundColor 124 77 255 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('# elder bases' + "\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Elder' + "\n")
f.write('\tItemLevel >= 86' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + elder_str_86.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 124 77 255 255' + "\n")
f.write('\tSetBackgroundColor 124 77 255 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Elder' + "\n")
f.write('\tItemLevel = 85' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + elder_str_85.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 124 77 255 255' + "\n")
f.write('\tSetBackgroundColor 124 77 255 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Elder' + "\n")
f.write('\tItemLevel = 84' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + elder_str_84.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 124 77 255 255' + "\n")
f.write('\tSetBackgroundColor 124 77 255 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Elder' + "\n")
f.write('\tItemLevel = 83' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + elder_str_83.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 124 77 255 255' + "\n")
f.write('\tSetBackgroundColor 124 77 255 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write("# crusader bases" + "\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Crusader' + "\n")
f.write('\tItemLevel >= 86' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + crusader_str_86.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Crusader' + "\n")
f.write('\tItemLevel = 85' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + crusader_str_85.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Crusader' + "\n")
f.write('\tItemLevel = 84' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + crusader_str_84.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Crusader' + "\n")
f.write('\tItemLevel = 83' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + crusader_str_83.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('# hunter bases' + "\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Hunter' + "\n")
f.write('\tItemLevel >= 86' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + hunter_str_86.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Hunter' + "\n")
f.write('\tItemLevel = 85' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + hunter_str_85.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Hunter' + "\n")
f.write('\tItemLevel = 84' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + hunter_str_84.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Hunter' + "\n")
f.write('\tItemLevel = 83' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + hunter_str_83.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('# redeemer bases' + "\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Redeemer' + "\n")
f.write('\tItemLevel >= 86' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + redeemer_str_86.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Redeemer' + "\n")
f.write('\tItemLevel = 85' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + redeemer_str_85.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Redeemer' + "\n")
f.write('\tItemLevel = 84' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + redeemer_str_84.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Redeemer' + "\n")
f.write('\tItemLevel = 83' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + redeemer_str_83.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('# warlord bases' + "\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Warlord' + "\n")
f.write('\tItemLevel >= 86' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + warlord_str_86.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Warlord' + "\n")
f.write('\tItemLevel = 85' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + warlord_str_85.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Warlord' + "\n")
f.write('\tItemLevel = 84' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + warlord_str_84.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tHasInfluence Warlord' + "\n")
f.write('\tItemLevel = 83' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + warlord_str_83.strip() + "\n")
f.write('\tSetTextColor 0 0 0 255' + "\n")
f.write('\tSetBorderColor 100 255 218 255' + "\n")
f.write('\tSetBackgroundColor 100 255 218 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('# normal bases' + "\n")

f.write('Show' + "\n")
f.write('\tItemLevel >= 86' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + normal_str_86.strip() + "\n")
f.write('\tSetTextColor 38 38 38 255' + "\n")
f.write('\tSetBorderColor 38 38 38 255' + "\n")
f.write('\tSetBackgroundColor 255 255 255 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tItemLevel = 85' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + normal_str_85.strip() + "\n")
f.write('\tSetTextColor 38 38 38 255' + "\n")
f.write('\tSetBorderColor 38 38 38 255' + "\n")
f.write('\tSetBackgroundColor 255 255 255 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tItemLevel = 84' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + normal_str_84.strip() + "\n")
f.write('\tSetTextColor 38 38 38 255' + "\n")
f.write('\tSetBorderColor 38 38 38 255' + "\n")
f.write('\tSetBackgroundColor 255 255 255 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('Show' + "\n")
f.write('\tItemLevel = 83' + "\n")
f.write('\tRarity <= Rare' + "\n")
f.write('\tBaseType ' + normal_str_83.strip() + "\n")
f.write('\tSetTextColor 38 38 38 255' + "\n")
f.write('\tSetBorderColor 38 38 38 255' + "\n")
f.write('\tSetBackgroundColor 255 255 255 255' + "\n")
f.write('\tSetFontSize 45' + "\n")
f.write('\tPlayAlertSound 6 300' + "\n")
f.write('\tMinimapIcon 0 Yellow Star' + "\n")
f.write('\tPlayEffect Green' + "\n\n")

f.write('There are {0} normal bases worth more than 20c, and {1} normal bases were ignored\n'.format(normal, normal_ignore))
f.write('There are {0} elder bases worth more than 20c, and {1} elder bases were ignored\n'.format(elder, elder_ignore))
f.write('There are {0} shaper bases worth more than 20c, and {1} shaper bases were ignored\n'.format(shaper, shaper_ignore))
f.write('There are {0} crusader bases worth more than 20c, and {1} crusader bases were ignored\n'.format(crusader, crusader_ignore))
f.write('There are {0} hunter bases worth more than 20c, and {1} hunter bases were ignored\n'.format(hunter, hunter_ignore))
f.write('There are {0} redeemer bases worth more than 20c, and {1} redeemer bases were ignored\n'.format(redeemer, redeemer_ignore))
f.write('There are {0} warlord bases worth more than 20c, and {1} warlord bases were ignored\n'.format(warlord, warlord_ignore))

f.write('There were {0} bases ignored due to low confidence'.format(confidence_ignore))
f.close()

end = time.time()
elapsed = end - start

print('Finished in {:+.2f}s check bases.txt'.format(elapsed))

# -------------------------------------------------------------------
# Uniques
# -------------------------------------------------------------------
start = time.time()
print('Fetching list of uniques from poe.ninja')


def uniques(lines):

    high = []
    mid = []
    low = []

    items = {}

    for i in lines:
        if 'links' in i:
            continue
        if i['chaosValue'] >= 20:
            high.append(i['baseType'])
        elif i['chaosValue'] >= 5 and i['chaosValue'] < 20:
            mid.append(i['baseType'])
        else:
            low.append(i['baseType'])

    high = list(dict.fromkeys(high))
    mid = list(dict.fromkeys(mid))
    low = list(dict.fromkeys(low))

    high.sort()
    mid.sort()
    low.sort()

    hideable = []
    for item in low:
        if item not in high:
            if item not in mid:
                hideable.append(item)

    top_tier = []
    for item in high:
        if item not in mid:
            if item not in low:
                top_tier.append(item)

    mid_tier = []
    for item in mid:
        if item not in low:
            mid_tier.append(item)


    items['top_tier'] = top_tier
    items['mid_tier'] = mid_tier
    items['hideable'] = hideable

    return items

high_list = []
mid_list = []
hideable = []

# amulets, rings, belts
print('-- accessories...')

req = urllib.request.Request('https://poe.ninja/api/data/itemoverview?league=' + league + '&type=UniqueAccessory', headers={'User-Agent': 'Mozilla/5.0'})
r = urllib.request.urlopen(req).read()
d = json.loads(r)

values = uniques(d['lines'])

high_list = high_list + values['top_tier']
mid_list = mid_list + values['mid_tier']
hideable = hideable + values['hideable']

# body armour, gloves, boots, helms, quivers, shields
print('-- armours...')

req = urllib.request.Request('https://poe.ninja/api/data/itemoverview?league=' + league + '&type=UniqueArmour', headers={'User-Agent': 'Mozilla/5.0'})
r = urllib.request.urlopen(req).read()
d = json.loads(r)

values = uniques(d['lines'])

high_list = high_list + values['top_tier']
mid_list = mid_list + values['mid_tier']
hideable = hideable + values['hideable']

# weapons
print('-- weapons...')

req = urllib.request.Request('https://poe.ninja/api/data/itemoverview?league=' + league + '&type=UniqueWeapon', headers={'User-Agent': 'Mozilla/5.0'})
r = urllib.request.urlopen(req).read()
d = json.loads(r)

values = uniques(d['lines'])

high_list = high_list + values['top_tier']
mid_list = mid_list + values['mid_tier']
hideable = hideable + values['hideable']

# flasks
print('-- flasks...')

req = urllib.request.Request('https://poe.ninja/api/data/itemoverview?league=' + league + '&type=UniqueFlask', headers={'User-Agent': 'Mozilla/5.0'})
r = urllib.request.urlopen(req).read()
d = json.loads(r)

values = uniques(d['lines'])

high_list = high_list + values['top_tier']
mid_list = mid_list + values['mid_tier']
hideable = hideable + values['hideable']

# some editing
high_list.append('Prismatic Jewel')
hideable.append('Elegant Round Shield')

high_list.sort()
mid_list.sort()
hideable.sort()

f = open('econ/uniques.txt', 'w')
f.write(' '.join('"{0}"'.format(w) for w in high_list).strip() + "\n\n")
f.write(' '.join('"{0}"'.format(w) for w in mid_list).strip() + "\n\n")
f.write(' '.join('"{0}"'.format(w) for w in hideable).strip() + "\n\n")

f.write('There were {0} high tier uniques, {1} mid tier uniques, and {2} hideable uniques'.format(len(high_list), len(mid_list), len(hideable)))
f.close()

end = time.time()
elapsed = end - start

print('Finished in {:+.2f}s, check uniques.txt'.format(elapsed))